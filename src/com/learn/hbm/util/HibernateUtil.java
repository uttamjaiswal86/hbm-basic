package com.learn.hbm.util;

import java.util.Properties;

import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

public class HibernateUtil {
	private static final SessionFactory SESSION_FACTORY;

	static {
		Configuration config = new Configuration();
		config.configure("hibernate.cfg.xml");
		StandardServiceRegistryBuilder builder = new StandardServiceRegistryBuilder();
		builder.applySettings(config.configure().getProperties());

		ServiceRegistry registry = builder.build();

		SESSION_FACTORY = config.buildSessionFactory(registry);
	}

	public static SessionFactory getSessionFactory() {
		if (SESSION_FACTORY != null)
			return SESSION_FACTORY;
		else
			return null;
	}
}
